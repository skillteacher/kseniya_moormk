using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLives;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private TextMeshProUGUI FinalCoinsCount;
    [SerializeField] private string firstLevelName = "Level 2";
    private int lives;
    private int coins;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        lives = startLives;
        coins = 0;
        ShowLives(lives);
        ShowCoins(coins);
    }

    public void LoseLive()
    {
        lives--;
        ShowLives(lives);
        if(lives <= 0)
        {
            lives = 0;

        }
        ShowLives(lives);
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        FinalCoinsCount.text = coins.ToString();
    }

    private void RestartGame()
    {
        lives = startLives;
        coins = 0;
        SceneManager.LoadScene(firstLevelName);
        ShowCoins(coins);
        mainMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins(coins);
    }

    private void ShowLives(int amount)
    {
        livesText.text = amount.ToString();
    }

    private void ShowCoins(int amount)
    {
        coinsText.text = amount.ToString();
    }
}
